fn main() {
    println!(
        "{:?}",
        solve("the quick brown fox jumps over the lazy dog", 10)
    );
}

fn solve(s: &str, k: i32) -> Vec<String> {
    let mut words = vec![];
    let split: Vec<&str> = s.split(' ').collect();

    let mut i = 0;
    let mut clump = String::new();
    while i < split.len() {
        if split[i].len() > k as usize {
            return vec![];
        }
        if clump.len() <= k as usize && split[i].len() + clump.len() < k as usize {
            if !clump.is_empty() {
                clump.push(' ');
            }
            clump.push_str(split[i]);
            i += 1;
        } else {
            words.push(clump.to_owned());
            clump = String::new();
        }
        if i == split.len() {
            words.push(clump.to_owned());
        }
    }

    words
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        let s = "the quick brown fox jumps over the lazy dog";
        assert_eq!(
            solve(s, 10),
            vec!["the quick", "brown fox", "jumps over", "the lazy", "dog"]
        );
    }

    #[test]
    fn test_2() {
        let s = "test test longer test";
        let expected: Vec<String> = vec![];
        assert_eq!(solve(s, 5), expected);
    }
}
